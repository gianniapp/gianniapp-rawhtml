@echo off
if [%1]==[] goto noparam
echo [make] Starting
set baselocation=%1
echo [make] Emptying build folder
rmdir /S /Q build
mkdir build
echo [make] Recreating .gitkeep
echo.>build\.gitkeep
echo [make] Copying base
xcopy /Y /E %baselocation%\* build
echo [make] Copying middler.js
copy /Y middler.js build\middler.js
echo [make] Copying redirect.html
copy /Y redirect.html build\redirect.html
echo [make] Removing .git folder
rmdir /S /Q build\.git
echo [make] Done
goto end
:noparam
echo [make] Usage: make.bat ^<path to base^>
exit /B 1
:end
